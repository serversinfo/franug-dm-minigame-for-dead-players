#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <devzones>
//#include <smlib>

#define PLUGIN_VERSION "v3.0"

new bool:g_dm[MAXPLAYERS+1] = {false, ...};
new bool:noarmas[MAXPLAYERS+1] = {false, ...};
new bool:cerrado = false;
new g_offsCollisionGroup;


new g_iOffset_PlayerResource_Alive = -1;

public Plugin:myinfo =
{
	name = "SM Franug DeathMach Minigame",
	author = "Franc1sco Steam: franug",
	description = "",
	version = PLUGIN_VERSION,
	url = "http://steamcommunity.com/id/franug"
};

public OnPluginStart()
{
	CreateConVar("sm_franugdmminigame_version", PLUGIN_VERSION, "", FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	g_offsCollisionGroup = FindSendPropOffs("CBaseEntity", "m_CollisionGroup");
	RegConsoleCmd("sm_dm", Command_dm);
	RegConsoleCmd("drop", drop13);
	
	AddNormalSoundHook(Hook_NormalSound);
	HookEvent("round_start", roundStart);
	HookEvent("round_end", Event_Round_End);
	HookEvent("player_death", Event_PlayerDeath);
	HookEvent("player_death", Event_PlayerDeath2,EventHookMode_Pre);
	HookEvent("player_footstep", Event_PlayerFootstep, EventHookMode_Pre);
	g_iOffset_PlayerResource_Alive = FindSendPropInfo("CCSPlayerResource", "m_bAlive");
	
	for (new i = 1; i < GetMaxClients(); i++)
		if (IsClientInGame(i)) OnClientPutInServer(i);
	CreateTimer(15.0, Message, _, TIMER_REPEAT);
}

public Action:Message(Handle:timer)
{
	for (new i = 1; i < GetMaxClients(); i++)
	{
		if (IsValidClient(i)  &&  g_dm[i])
			PrintToChat(i, "Чтобы выйти из DM введите !dm в чат");
	}
}

/* #if _USEPROXY
public OnMapStart()
{
	g_iPlayerManager = FindEntityByClassname(0, "cs_player_manager");
}
#endif */

public OnMapStart()
{
	new entity = FindEntityByClassname(0, "cs_player_manager");
	SDKHook(entity, SDKHook_ThinkPost, OnPlayerManager_ThinkPost);
}

public OnPlayerManager_ThinkPost(entity)
{
	for (new i = 1; i < GetMaxClients(); i++)
	{
		if(g_dm[i])
		{
			SetEntData(entity, (g_iOffset_PlayerResource_Alive+i*4), 0, 1, true);
		}
	}
}

public OnClientDisconnect(client)
{
	g_dm[client] = false;
	noarmas[client] = false;
}

public Action:drop13(client, args)
{
	if(g_dm[client]) return Plugin_Handled;
	
	return Plugin_Continue;
}

public Action:Command_dm(client, args)
{
	PrintToChatAll("Action:Command_dm client == %i", client);
	decl Float:Position[3];
	if(!Zone_GetZonePosition("dmzone", false, Position)) return Plugin_Continue;
	
	if (!cerrado  &&  !IsPlayerAlive(client)  &&  !g_dm[client]  &&  IsValidClient(client)  &&  (GetClientTeam(client) == 2 || GetClientTeam(client) == 3))
	{
		g_dm[client] = true;
		
		CS_SetClientClanTag(client, "*DEAD*");
		CreateTimer(0.5, Resucitar, GetClientUserId(client));
		PrintToChat(client,"Вы отпраляетесь на DM");
	}
	else if (g_dm[client])
	{
		PrintToChat(client,"Оказано, вы и так играете в ДМ");
		if (IsClientInGame(client))
		{
			if(g_dm[client])
			{
				if(IsPlayerAlive(client))
				{
					ForcePlayerSuicide(client);			// убиваем и сохраняем фраги и смерти
					SetEntProp(client, Prop_Data, "m_iFrags", GetClientFrags(client)+1);
					new olddeaths = GetEntProp(client, Prop_Data, "m_iDeaths");
					SetEntProp(client, Prop_Data, "m_iDeaths", olddeaths-1);
				}

				g_dm[client] = false;
				CS_SetClientClanTag(client, "");
			}
		}
	}
	else if (IsPlayerAlive(client))
		PrintToChat(client,"Оказано, вы живы");
	else if (cerrado)
		PrintToChat(client,"Оказано, cerrado == true");
	else if (!(GetClientTeam(client) == 2 || GetClientTeam(client) == 3))
		PrintToChat(client,"Оказано, вы должны быть в команде");

	return Plugin_Handled;
}

public Action:Event_PlayerDeath2(Handle:event, const String:name[], bool:dontBroadcast)
{
	new victim = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(g_dm[victim])
		return Plugin_Handled;

	return Plugin_Continue;
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
/*
	if(jugadores == 1 && IsValidClient(ultimo))
	{
		PrintToChatAll("SOLO QUEDA UN CT, AHORA ESTE PUEDE ESCRIBIR !ceo para empezar ronda especial");
		ceo = true;
	}
*/
	decl Float:Position[3];
	if(!Zone_GetZonePosition("dmzone", false, Position)) return Plugin_Continue;
	

	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(g_dm[client])
	{
		CreateTimer(2.0, Resucitar, GetClientUserId(client));
		PrintToChatAll("CreateTimer(2.0, Resucitar, %i);", client);
		return Plugin_Continue;
	}
	else if(!cerrado)
		DID2(client);

	new cts = 0;
	new terros2 = 0;
	for (new i = 1; i < GetMaxClients(); i++)	//считаем живых КТ и Т 
	{
		if (IsClientInGame(i) && IsPlayerAlive(i))
		{
			if(GetClientTeam(i) == 2 && !g_dm[i])
				terros2++;
			else if(GetClientTeam(i) == 3 && !g_dm[i])
				cts++;
		}
	}

	if(terros2 <= 1 || cts == 0)
	{
		cerrado = true;
		for (new i = 1; i < GetMaxClients(); i++)	// выгоняем всех из ДМ
		{
			if (IsClientInGame(i))
			{
				if(g_dm[i])
				{
					if(IsPlayerAlive(i))
					{
						ForcePlayerSuicide(i);											// убиваем и сохраняем фраги и смерти
						SetEntProp(i, Prop_Data, "m_iFrags", GetClientFrags(i)+1);
						new olddeaths = GetEntProp(i, Prop_Data, "m_iDeaths");
						SetEntProp(i, Prop_Data, "m_iDeaths", olddeaths-1);
					}

					g_dm[i] = false;
					CS_SetClientClanTag(i, "");
				}
			}
		}
	}
	return Plugin_Continue;
}

public Action:DID2(clientId) 
{
	new Handle:menu2 = CreateMenu(DIDMenuHandler2);
	SetMenuTitle(menu2, "DM minigame\nВы хотите сыграть в DM?");
	AddMenuItem(menu2, "opcion1", "ДА");
	AddMenuItem(menu2, "opcion2", "НЕТ");
	SetMenuExitButton(menu2, true);
	DisplayMenu(menu2, clientId, 15);

	return Plugin_Handled;
}

public DIDMenuHandler2(Handle:menu, MenuAction:action, client, itemNum) 
{
	if ( action == MenuAction_Select ) 
	{
		new String:info[32];
		
		GetMenuItem(menu, itemNum, info, sizeof(info));
		
		if (strcmp(info,"opcion1") == 0 && !cerrado && !IsPlayerAlive(client))
		{
			g_dm[client] = true;

			CS_SetClientClanTag(client, "*DEAD*");
			CreateTimer(0.5, Resucitar, GetClientUserId(client));
			PrintToChatAll("CreateTimer(0.5, Resucitar, GetClientUserId(%i))", client);
		}
	}
	else if (action == MenuAction_Cancel) 
		PrintToChatAll("Client %d's menu was cancelled.  Reason: %d", client, itemNum); 
		
	else if (action == MenuAction_End)
		CloseHandle(menu);
}

public Action:Event_Round_End(Handle:event, const String:name[], bool:dontBroadcast)
{
	cerrado = true;
	for (new i = 1; i < GetMaxClients(); i++)
	{
		if (IsClientInGame(i))
		{
			if(IsPlayerAlive(i) && g_dm[i])
			{
				ForcePlayerSuicide(i);
				g_dm[i] = false;
				noarmas[i] = false;
			}
			CS_SetClientClanTag(i, "");
		}
	}
}

public Action:Resucitar(Handle:timer,any:userid)
{
	new client = GetClientOfUserId(userid);
	
	PrintToChatAll("Action:Resucitar client == %i userid ==  %i", client, userid);
	if(IsValidClient(client) && (GetClientTeam(client) == 2 || GetClientTeam(client) == 3) && g_dm[client] && !cerrado && !IsPlayerAlive(client))
	{
		PrintToChatAll("CS_RespawnPlayer %i", client);
		CS_RespawnPlayer(client);
		SetEntData(client, g_offsCollisionGroup, 2, 4, true);
		SetEntProp(client, Prop_Send, "m_lifeState", 0);
		decl Float:Position[3];
		if(GetClientTeam(client) == 2)
		{
			noarmas[client] = false;
			
			GivePlayerItem(client, "weapon_m4a1");
			
			GivePlayerItem(client, "weapon_knife");
			if(Zone_GetZonePosition("dm1", false, Position)) TeleportEntity(client, Position, NULL_VECTOR, NULL_VECTOR);
			noarmas[client] = true;
		}
		else if(GetClientTeam(client) == 3)
		{
			noarmas[client] = false;
			
			GivePlayerItem(client, "weapon_m4a1");
			
			GivePlayerItem(client, "weapon_knife");
			if(Zone_GetZonePosition("dm2", false, Position)) TeleportEntity(client, Position, NULL_VECTOR, NULL_VECTOR);
			noarmas[client] = true;
		}
	}
	else if (GetClientTeam(client) < 2 )
		PrintToChat(client,"Вы должны быть в команде");
	else if (!g_dm[client])
		PrintToChat(client,"Вы и так играете в ДМ");
	else if (cerrado)
		PrintToChat(client,"cerrado == true");
	else if (IsPlayerAlive(client))
		PrintToChat(client,"IsPlayerAlive(client)");
}

public Action:roundStart(Handle:event, const String:name[], bool:dontBroadcast) 
{
	cerrado = false;

	for (new i = 1; i < GetMaxClients(); i++)
	{
		if (IsClientInGame(i))
		{
			if(g_dm[i])
				g_dm[i] = false;

			noarmas[i] = false;
		}
	}
}

public IsValidClient( client ) 
{
	if ( !( 1 <= client <= MaxClients ) || !IsClientInGame(client) )
		return false;

	return true;
}

public Zone_OnClientLeave(client, String:zone[])
{
	if(StrContains(zone, "dmzone", false) == 0 && IsValidClient(client) && g_dm[client] && !cerrado)
	{
		PrintToChat(client, "\x03Dont go out of DeathMach!");
		decl Float:Position[3];

		if(GetClientTeam(client) == CS_TEAM_CT)
		{
			if(Zone_GetZonePosition("dm2", false, Position))
				TeleportEntity(client, Position, NULL_VECTOR, NULL_VECTOR);
		}

		else if(GetClientTeam(client) == CS_TEAM_T)
		{
			if(Zone_GetZonePosition("dm1", false, Position))
				TeleportEntity(client, Position, NULL_VECTOR, NULL_VECTOR);
		}

	}
}

public OnClientPutInServer(client)
{
	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
	SDKHook(client, SDKHook_WeaponDropPost, OnWeaponDrop);
	SDKHook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
	SDKHook(client, SDKHook_SetTransmit, Hook_SetTransmit); 
}

public Action:OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype)
{
	if(!IsValidClient(attacker)) return Plugin_Continue;
	
	if(g_dm[victim] != g_dm[attacker])
	{
		//PrintToChat(attacker, "\x03No podeis atacaros si ambos no estais en DM!");
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public OnWeaponDrop(client, entity)
{
	if (!IsClientInGame(client) || !IsValidEdict(entity) || GetClientHealth(client) > 0 || !g_dm[client])
		return;

	AcceptEntityInput(entity, "kill");
}

public Action:OnWeaponCanUse(client, weapon)
{
	if (noarmas[client]) return Plugin_Handled;
	
	return Plugin_Continue;
}

public Action:Hook_SetTransmit(entity, client) 
{
	if (entity != client && g_dm[client] != g_dm[entity] && IsPlayerAlive(client)) 
		return Plugin_Handled;
	
	return Plugin_Continue; 
}


public Action:Hook_NormalSound(clients[64], &numClients, String:sample[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags)
{
	// Ignore non-weapon sounds.
	if (!(strncmp(sample, "weapons", 7) == 0 || strncmp(sample[1], "weapons", 7) == 0))
		return Plugin_Continue;
		
		
	//PrintHintTextToAll("es %i",entity);
	new client = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
	if(!IsValidClient(client) || !g_dm[client])
		return Plugin_Continue;
	
	decl i, j;
	
	for (i = 0; i < numClients; i++)
	{
		if (!g_dm[clients[i]])
		{
			// Remove the client from the array.
			for (j = i; j < numClients-1; j++)
				clients[j] = clients[j+1];
			
			numClients--;
			i--;
		}
	}
	
	return (numClients > 0) ? Plugin_Changed : Plugin_Stop;
}

public Action:Event_PlayerFootstep(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
/* 	if (!Client_IsValid(client)) {
		return Plugin_Continue;
	} */
	return g_dm[client] ? Plugin_Handled : Plugin_Continue;
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if(g_dm[client])
		buttons &= ~IN_USE;
	
	return Plugin_Changed;
}